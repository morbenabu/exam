<?php

use yii\db\Migration;

class m160726_054350_deal_table extends Migration
{
    public function up()
    {
			$this->createTable(
            'deal',
            [
                'id' => 'pk',
                'leadId' => 'int',
                'name' => 'string',
                'amount' => 'int',
            ],
            'ENGINE=InnoDB'
        );

    }

    public function down()
    {
        $this->dropTable('deal');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
